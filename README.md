> JSON is lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate.
>
> -- <cite>[JSON.org](https://www.json.org/)</cite>

The MGI JSON parser is optimized for speed and programming convenience. It maximizes your ability to move between JSON strings and LabVIEW data types.

This library is more flexible than the built in JSON parser. It handles conversion from almost all LabVIEW data types, and allows you to more easily access data when the exact JSON format isn't known until runtime.

Supported data types are integers, floats, complex, enums, booleans, strings, some refnums, paths, arrays, clusters, timestamps, and nested combinations of these. Unsupported types can be automatically converted to/from string via flatten/unflatten if desired.

See Usage Examples.vi for the most common ways to use the library.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2020_
